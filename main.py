# -*- coding: utf-8 -*-
import cv2
import numpy as np
import os
import time
import argparse
import csv
import progressbar
import pytesseract
from PIL import Image

LANGUAGES = {'eng':'eng', 'vie':'script/Vietnamese', 'jpn':'script/Japanese'}

def preprocess_image(image):
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    
    gray = cv2.Canny(gray, 150, 230)
    gray = cv2.medianBlur(gray, 3)
    ret,  newimg = cv2.threshold(gray, 230, 255, cv2.THRESH_BINARY)
    kernel = cv2.getStructuringElement(cv2.MORPH_CROSS, (3,3))
    dilated = cv2.dilate(newimg, kernel, iterations=9)
    
    return dilated

def preprocess_text(text):
    numbers = ['0','1','2','3','4','5','6','7','8','9','$','%']
    text = text.replace('\r', '')
    lines = text.split('\n')
    newtext = ""
    for line in lines:
        found = 0
        for ch in line:
            if ch in numbers:
                found += 1    
        if found < 3:
            newtext = newtext + line
    newtext = newtext.replace('\n','').replace(' ','')
    return newtext

def find_contours(image, loop_cnt, dilate_cnt):
    _, contours, hierarchy = cv2.findContours(image, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
    
    (H, W) = image.shape[:2]
    black = np.zeros((H, W), np.uint8)
    kernel = cv2.getStructuringElement(cv2.MORPH_CROSS, (3,3))
    for i in range(loop_cnt):
        for cont in contours:
            [x, y, w, h] = cv2.boundingRect(cont)
            cv2.rectangle(black, (x, y), (x+w, y+h), (255,255,255), cv2.FILLED)
            
        black = cv2.dilate(black, kernel, iterations=2)
        _, contours, hierarchy = cv2.findContours(black, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)
        
    return contours

def axis_separate(boxes, axis): #axis = 0 => x axis = 1 => y
    boxes = np.array(boxes).reshape((-1, 4))
    sorted_boxes = boxes[boxes[:,axis].argsort()]
    
    soft_border = 0
    if axis == 0:
        soft_border = -soft_border
    
    
    o_axis = 0
    if axis == 0:
        o_axis = 1
        
    blocks = []
    cur_region = []
    cur_start = -1
    cur_end = -1
    for box in sorted_boxes:
        ns = box[axis]
        ne = box[axis+2]
        if ns <= cur_end + soft_border:
            if ne > cur_end:
                cur_end = ne
            cur_region.append(box.copy())
        else:
            if len(cur_region) != 0:
                cur_region_np = np.array(cur_region).reshape((-1, 4))
                cur_o_start = min(cur_region_np[:,o_axis])
                cur_o_end = max(cur_region_np[:,o_axis+2])
                if axis == 0:
                    blocks.append([[cur_start, cur_o_start, cur_end, cur_o_end], cur_region.copy()])
                else:
                    blocks.append([[cur_o_start, cur_start, cur_o_end, cur_end], cur_region.copy()])
            cur_start = ns
            cur_end = ne
            cur_region.clear()
            cur_region.append(box.copy())
            
    if len(cur_region) != 0:
        cur_region_np = np.array(cur_region).reshape((-1, 4))
        cur_o_start = min(cur_region_np[:,o_axis])
        cur_o_end = max(cur_region_np[:,o_axis+2])
        if cur_start < 0:
            cur_start = 0
        if axis == 0:
            blocks.append([[cur_start, cur_o_start, cur_end, cur_o_end], cur_region.copy()])
        else:
            blocks.append([[cur_o_start, cur_start, cur_o_end, cur_end], cur_region.copy()])
            
    return blocks

def layout_separate(boxes):
    blocks = []
    v_blocks = axis_separate(boxes, 1)
    
    for vb in v_blocks:
        h_blocks = axis_separate(vb[1], 0)
        for hb in h_blocks:
            blocks.append(hb.copy())
    
    return blocks

def score_box(box, image, lang):
    # Fundamental properties
    [bl, bt, br, bb] = box
    [pl, pt, pr, pb] = [0, 0, image.shape[1], image.shape[0]]
    bw = br - bl
    bh = bb - bt
    pw = pr - pl
    ph = pb - pt
    ctr_box = (bl + int(bw/2), bt + int(bh/2))
    ctr_par = (pl + int(pw/2), pt + int(ph/2))
    
    # Synthesized properties
    # Relative coordinates of the box centre 
    rel_ctr_x = np.abs(ctr_box[0]-ctr_par[0]) / pw *2
    rel_ctr_y = np.abs(ctr_box[1]-ctr_par[1]) / ph *2
    # Areas
    area_box = bw*bh
    area_par = pw*ph
    rel_area = area_box / area_par
    # Text
    cropped = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)[bt:bb,bl:br]
    cropped = cv2.resize(cropped, (int(cropped.shape[1]/2), int(cropped.shape[0]/2)))
    text = extract_text(cropped, lang)
    newtext = preprocess_text(text)
    
    params = [3, 1, 1, 6]
    props = [rel_area, my_gauss(bw/pw,1,2.0), my_gauss(rel_ctr_x,0,0.5), my_sigmoid(np.log10(len(newtext)+1), -1)]
    deter_rate = 1.
    if pw < ph and (bt < 0.066*ph or bb > 0.95*ph):
        deter_rate = 0.5
    score = np.sum(np.multiply(props, params)) / np.sum(params) * deter_rate
    properties = [bl/pw, bt/ph, br/pw, bb/ph]
    return [properties, score, text]

def my_gauss(x, mu, sig):
    return (1. / (np.sqrt(2.*np.pi)*sig) * np.exp(-np.power((x - mu)/sig, 2.) / 2.))

def my_sigmoid(x, offset_x):
    return (1. / (1. + np.exp(-x - offset_x)))

def normalize(scores):
    maxval = np.max(scores)
    return np.divide(scores, maxval)
    
def extract_text(cropped_img, lang):
    cv2.imwrite("temp.png", cropped_img)
    text = pytesseract.image_to_string(Image.open("temp.png"), LANGUAGES[lang])
    os.remove("temp.png")
    
    return text

""" =================== MAIN ===================== """
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--input", type=str, help="path to input image folder")
ap.add_argument("-d", "--demo", type=str, default="", help="path to output image folder")
ap.add_argument("-o", "--output", type=str, default=".", help="path to output csv files")
ap.add_argument("-t", "--threshold", type=float, default=0.8, help="default threshold for rejecting unimportant blocks")
args = ap.parse_args()


IMAGE_PATH = args.input
CSV_PATH = args.output
DEMO_PATH = args.demo
THRESHOLD = args.threshold

for p in os.listdir(IMAGE_PATH):
    start_time = time.time()
    
    """ Preprocessing images """
    image = cv2.imread(os.path.join(IMAGE_PATH, p))
    ct = time.time()
    (H, W) = image.shape[:2]
    preprocessed = preprocess_image(image)
    
    """ Find boxes"""
    contours = find_contours(preprocessed, 5, 2) 
    boxes = []
    for cont in contours:
        [x, y, w, h] = cv2.boundingRect(cont)
        if (w*h > 5000):
            boxes.append([x, y, x+w, y+h])
    
    #""" Layout Separating """
    #first_level_layout = layout_separate(boxes)
    #first_level_boxes = []
    
    """ Scoring """
    properties = []
    scores = []
    texts = []
    for block in boxes:
        lang = p.split('_')[1].split('.')[0]
        [prop, score, text] = score_box(block, image, lang)
        scores.append(score)
        properties.append(prop)
        texts.append(text)
    scores = normalize(scores)
    
    """ Export csv """
    csv_file_name = CSV_PATH + '/' + p.split('.')[0] + '.csv'
    with open(csv_file_name, mode='w', encoding='utf-8') as csv_file:
        writer = csv.writer(csv_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        line = ['left', 'top', 'right', 'bottom', 'score', 'text']
        writer.writerow(line)
        for idx in range(len(properties)):
            line.clear()
            line.extend(properties[idx])
            line.append(scores[idx])
            line.append('"' + texts[idx] + '"')
            writer.writerow(line)
    
    
    """ Demo """
    if args.demo != "":
        img = image.copy()
        for idx, block in enumerate(boxes):
            [l, t, r, b] = block
            score = scores[idx]
            if score < THRESHOLD:
                cv2.rectangle(img, (l, t), (r, b), (0,0,255), 10)
            else:
                cv2.rectangle(img, (l, t), (r, b), (0,255,0), 10)
            cv2.putText(img, str(round(score, 5)), (l+10,b-10), cv2.FONT_HERSHEY_COMPLEX, 2, (255,0,255), 2, cv2.LINE_AA)
            
        demo_file_name = DEMO_PATH + "/" + p.split('.')[0] + '.png'
        cv2.imwrite(demo_file_name, img)
        
    print(p, ": ", time.time() - start_time)
    
cv2.destroyAllWindows()
    